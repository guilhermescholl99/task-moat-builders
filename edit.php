<?php
    include_once('conexao.php');

    if(!empty($_GET['album_id']))
    {
        $album_id = $_GET['album_id'];
        $sqlSelect = "SELECT * FROM album_list WHERE album_id=$album_id";
        $result = $conexao->query($sqlSelect);
        if($result->num_rows > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                $artist = $row['artist'];
                $album_name = $row['album_name'];
                $year = $row['year'];
                
            }
        }
        else
        {
            header('Location: painel_admin.php');
        }
    }
    else
    {
        header('Location: painel_admin.php');
    }
?>
<!DOCTYPE html>
<html>
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fly Music</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
    <section class= "hero is-primary is-medium is-bold ">
            <div class = "hero-head">
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-brand">
                            <a href="painel.php" class="navbar-item">
                                <h3 class="title is-3">Fly Music</h3>
                            </a>
                                
                        </div>
                    </div>
                </nav>
                
            </div>
        </section>  
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <div class="notification is-success">
                            <p>View all albums <a href="painel_admin.php">Here</a></p>
                    </div>
                    <div class="notification is-info">
                        <p>Edit your album!</p>
                    </div>
                    <div class="box">
                        <form action="editar.php"  method="POST">
                            <div class="field">
                                <div class="control">
                                    <div class="select is-large">
                                        <select name="artist">
                                            <option>Justin Bieber</option>
                                            <option>Katy Perry</option>
                                            <option>Rihanna</option>
                                            <option>Taylor Swift</option>
                                            <option>Lady Gaga</option>
                                            <option>Ariana Grande</option>
                                            <option>Justin Timberlake</option>
                                            <option>Selena Gomez</option>
                                            <option>Britney Spears</option>
                                            <option>Demi Lovato</option>
                                            <option>Shakira</option>
                                            <option>Jennifer Lopez</option>
                                            <option>Miley Cyrus</option>
                                            <option>Bruno Mars</option>
                                            <option>Niall Horan</option>
                                            <option>Drake</option>
                                            <option>Wiz Khalifa</option>
                                            <option>Harry Styles</option>
                                            <option>Lil Wayne</option>
                                            


                                        
                                        </select>
                                    </div>    
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input name="album_name" type="text" class="input is-large" value=<?php echo $album_name;?> required>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input name="year" class="input is-large" type="input is-large" value=<?php echo $year;?>>
                                </div>
                            </div>
                            <input type="hidden" name="album_id" value=<?php echo $album_id;?>>
                            <button type="submit" name="update" class="button is-block is-link is-large is-fullwidth">Edit Album</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

</html>