<?php
include('verifica_login.php');
include('conexao.php')
?>
<!DOCTYPE html>
 <html>

 <head>
 	<style type="text/css">
 		section {
 			padding-top: 10px;
 		}

 	</style>
       <title>Fly Music</title>
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fly Music</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="css/login.css">
 </head>
 <body>
 	<div>
        <section class= "hero is-primary is-medium is-bold ">
            <div class = "hero-head">
                <nav class="navbar">
                    <div class="container">
                        	<div class="navbar-brand">
                            	<a  href="" class="navbar-item">
                                <h3 class="title is-3">Fly Music</h3>
                            	</a>
                        	</div>
                        	<div class="navbar-end">   
                        		<a class="navbar-item">
                                	<h2 class="title is-5">Hello, <?php echo $_SESSION['nome'];?></h2>
                            	</a>
                            	<a href="cadastro_album.php" class="navbar-item">
									<h2 class="title is-5">Register Album</h2>
                            	</a>
                            	<a href="logout.php" class="navbar-item">
									<h2 class="title is-5">Exit</h2>
                            	</a>
                            	

                            	
                        	</div>     
                        </div>
                    </div>
                </nav>
                
            </div>
        </section> 
        <section class= "hero is-medium is-bold has-background-grey-lighter ">
        	<div class="Hero-head">
        		<h1 class="title is-5 is-medium">Artists available:</h1>
        	</div>
        </section> 
		<section class="hero is-medium is-bold has-background-grey-lighter">
			
				<div class="columns">
				 	<?php

				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => 'http://www.moat.ai/api/task',
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
				  CURLOPT_CUSTOMREQUEST => 'GET',
				  CURLOPT_HTTPHEADER => array(
				    'Basic: ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=='
				  ),
				));

				$response = curl_exec($curl);
				$infor = json_decode($response, true);
				for ($i=0; $i < 19; $i++) { 
					$id = $infor[$i];
					$in = $id[0];
					$name = $in['name'];
					echo "<div class='column tag is-primary is-light'>$name</div>";
					

				}
				
				curl_close($curl);
				?>
			</div>
	  	</section>
		<section class="hero is-medium is-fullheight has-background-grey-lighter">
					<div class="container">
						
						  <?php
									$sql = "SELECT album_id, artist, album_name, year FROM album_list";
										$result = $conexao->query($sql);

										if ($result->num_rows > 0) {
    					
    									while($row = $result->fetch_assoc()) {
    					
       									 	echo "<div class='box  column is-8 is-offset-2 '>";
       									 	
       									 	echo "<br>". $row["artist"]."<br>";
       									 	
       					  					echo "<br>". $row["album_name"]."<br>";
       					  					
       					   					echo "<br>". $row["year"] . "<br>";
       					   					
       					 					echo "<a class='button is-primary' href='edit.php?album_id=$row[album_id]'>
       					 								<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-pencil-square' viewBox='0 0 16 16'>
  															<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/>
  															<path fill-rule='evenodd' d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/>
															</svg></a>";
       					 					
       										echo"<a class='button is-danger' href='delete.php?album_id=$row[album_id]' >
       													<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' 	class='bi bi-trash3' viewBox='0 0 16 16'>
  														<path d='M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z'/>
															</svg></a>";

											echo "</div>";
       									
					    					}
									} else {
					    						echo "0 results";
										}
									?>
						
					
				</div>
				
				
				
				</div>
			</div>
       </section>
       
       
 </body>
 </html>

