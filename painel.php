<?php
include('verifica_login.php');
include('conexao.php')
?>
<!DOCTYPE html>
 <html>

 <head>
 	<style type="text/css">
 		section {
 			padding-top: 10px;
 		}
 		
 	</style>
       <title>Fly Music</title>
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fly Music</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="css/login.css">
 </head>
 <body>
 	<div>
        <section class= "hero is-primary is-medium is-bold ">
            <div class = "hero-head">
                <nav class="navbar">
                    <div class="container">
                        	<div class="navbar-brand">
                            	<a  href="" class="navbar-item">
                                <h3 class="title is-3">Fly Music</h3>
                            	</a>
                        	</div>
                        	<div class="navbar-end">   
                        		<a class="navbar-item">
                                	<h2 class="title is-5">Hello, <?php echo $_SESSION['nome'];?></h2>
                            	</a>
                            	
                            	<a href="logout.php" class="navbar-item">
									<h2 class="title is-5">Exit</h2>
                            	</a>
                            	

                            	
                        	</div>     
                        </div>
                    </div>
                </nav>
                
            </div>
        </section> 
        <section class= "hero is-medium is-bold has-background-grey-lighter ">
        	<div class="Hero-head">
        		<h1 class="title is-5 is-medium">Artists available:</h1>
        	</div>
        </section> 
		<section class="hero is-medium is-bold has-background-grey-lighter">
			
				<div class="columns">
				 	<?php

				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => 'http://www.moat.ai/api/task',
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
				  CURLOPT_CUSTOMREQUEST => 'GET',
				  CURLOPT_HTTPHEADER => array(
				    'Basic: ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=='
				  ),
				));

				$response = curl_exec($curl);
				$infor = json_decode($response, true);
				for ($i=0; $i < 19; $i++) { 
					$id = $infor[$i];
					$in = $id[0];
					$name = $in['name'];
					echo "<div class='column tag is-primary is-light'>$name</div>";
					

				}
				
				curl_close($curl);
				?>
			</div>
	  	</section>
		<section class="hero is-medium is-fullheight has-background-grey-lighter">
					<div class="container">
						
						  <?php
									$sql = "SELECT album_id, artist, album_name, year FROM album_list";
										$result = $conexao->query($sql);

										if ($result->num_rows > 0) {
    					
    									while($row = $result->fetch_assoc()) {
    					
       									 	echo "<div class='box  column is-8 is-offset-2 '>";
       									 	
       									 	echo "<br>". $row["artist"]."<br>";
       									 	
       					  					echo "<br>". $row["album_name"]."<br>";
       					  					
       					   					echo "<br>". $row["year"] . "<br>";
       					   					
       					 					

											echo "</div>";
       									
					    					}
									} else {
					    						echo "0 results";
										}
									?>
						
					
				</div>
				
				
				
				</div>
			</div>
       </section>
       
       
 </body>
 </html>

