<!DOCTYPE html>
<html>
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fly Music</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
    <section class= "hero is-primary is-medium is-bold ">
            <div class = "hero-head">
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-brand">
                            <a href="painel.php" class="navbar-item">
                                <h3 class="title is-3">Fly Music</h3>
                            </a>
                                
                        </div>
                    </div>
                </nav>
                
            </div>
        </section>  
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <div class="notification is-success">
                            <p>View all albums <a href="painel_admin.php">Here</a></p>
                    </div>
                    <div class="notification is-info">
                        <p>Register your album!</p>
                    </div>
                    <div class="box">
                        <form action="cadastrar_album.php" method="POST">
                            <div class="field">
                                <div class="control">
                                    <div class="select is-large">
                                        <select name="artist">
                                            <option>Justin Bieber</option>
                                            <option>Katy Perry</option>
                                            <option>Rihanna</option>
                                            <option>Taylor Swift</option>
                                            <option>Lady Gaga</option>
                                            <option>Ariana Grande</option>
                                            <option>Justin Timberlake</option>
                                            <option>Selena Gomez</option>
                                            <option>Britney Spears</option>
                                            <option>Demi Lovato</option>
                                            <option>Shakira</option>
                                            <option>Jennifer Lopez</option>
                                            <option>Miley Cyrus</option>
                                            <option>Bruno Mars</option>
                                            <option>Niall Horan</option>
                                            <option>Drake</option>
                                            <option>Wiz Khalifa</option>
                                            <option>Harry Styles</option>
                                            <option>Lil Wayne</option>
                                            


                                        
                                        </select>
                                    </div>    
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input name="album_name" type="text" class="input is-large" placeholder="Enter the album name">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input name="year" class="input is-large" type="input is-large" placeholder="Enter the Year of the album">
                                </div>
                            </div>
                            <button type="submit" class="button is-block is-link is-large is-fullwidth">Register Album</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

</html>